use std::path::Path;

use anyhow::anyhow;
use anyhow::Result;

use openpgp::cert::CertParser;
use openpgp::packet::UserID;
use openpgp::parse::Parse;
use openpgp::policy::NullPolicy;
use openpgp::Cert;
use sequoia_openpgp as openpgp;

use sequoia_wot as wot;

use crate::cli::types::FileOrStdin;
use crate::load_certs;
use crate::output::sanitize::Safe;

mod revoke;
pub use revoke::get_secret_signer;
pub use revoke::RevocationOutput;

pub mod password;
pub mod userid;

pub const NULL_POLICY: &NullPolicy = &NullPolicy::new();

/// Parse the cert from input and ensure it is only one cert.
pub fn read_cert(input: Option<&Path>) -> Result<Cert> {
    let input = FileOrStdin::from(input).open()?;

    let cert = CertParser::from_buffered_reader(input)?.collect::<Vec<_>>();
    let cert = match cert.len() {
        0 => Err(anyhow!("No certificates provided."))?,
        1 => cert.into_iter().next().expect("have one")?,
        _ => Err(anyhow!("Multiple certificates provided."))?,
    };
    Ok(cert)
}

/// Parse the secret key and ensure it is at most one.
pub fn read_secret(skf: Option<&Path>) -> Result<Option<Cert>> {
    let secret = load_certs(skf.into_iter())?;
    if secret.len() > 1 {
        Err(anyhow!("Multiple secret keys provided."))?;
    }
    let secret = secret.into_iter().next();
    Ok(secret)
}

/// Something like a User ID.
///
/// This is used to avoid unnecessary allocations.
enum UserIDLike {
    UserID(UserID),
    String(String),
    Unknown,
}

/// The preferred user ID for a certificate.
///
/// This can be smartly truncated using the precision formatting
/// parameter, e.g.:
///
/// ```text
/// format!("{:.70}", userid);
/// ```
pub struct PreferredUserID {
    userid: UserIDLike,
    trust_amount: usize,
}

impl std::fmt::Display for PreferredUserID {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>)
        -> Result<(), std::fmt::Error>
    {
        let userid_;
        let userid = match self.userid {
            UserIDLike::Unknown => {
                return write!(f, "<unknown>");
            }
            UserIDLike::UserID(ref userid) => {
                userid_ = String::from_utf8_lossy(userid.value());
                &userid_[..]
            }
            UserIDLike::String(ref userid) => {
                &userid[..]
            }
        };

        let userid = Safe(userid).to_string();

        let suffix_;
        let suffix = if self.trust_amount == 0 {
            "(UNAUTHENTICATED)"
        } else if self.trust_amount < wot::FULLY_TRUSTED {
            suffix_ = format!("(partially authenticated, {}/{})",
                              self.trust_amount, wot::FULLY_TRUSTED);
            &suffix_[..]
        } else {
            "(authenticated)"
        };

        // We always keep the suffix and at least 16 characters of the user ID.
        const MIN_USERID: usize = 16;

        if let Some(width) = f.precision() {
            let space_for_userid = width.saturating_sub(1 + suffix.len()).max(MIN_USERID);
            if userid.chars().count() > space_for_userid {
                return write!(f, "{}… {}",
                              userid.chars().take(MIN_USERID).collect::<String>(),
                              suffix);
            }
        }

        write!(f, "{} {}", userid, suffix)
    }
}

impl PreferredUserID {
    /// Returns a new `PreferredUserID`.
    pub fn from_userid<U>(userid: U, trust_amount: usize) -> Self
    where U: Into<UserID>
    {
        Self {
            userid: UserIDLike::UserID(userid.into()),
            trust_amount,
        }
    }

    /// Returns a new `PreferredUserID`.
    pub fn from_string<S>(userid: S, trust_amount: usize) -> Self
    where S: Into<String>
    {
        Self {
            userid: UserIDLike::String(userid.into()),
            trust_amount,
        }
    }

    /// Returns a new "unknown" `PreferredUserID`.
    pub fn unknown() -> Self {
        Self {
            userid: UserIDLike::Unknown,
            trust_amount: 0,
        }
    }

    /// Returns the trust amount.
    pub fn trust_amount(&self) -> usize {
        self.trust_amount
    }
}
