#![doc(html_favicon_url = "https://docs.sequoia-pgp.org/favicon.png")]
#![doc(html_logo_url = "https://docs.sequoia-pgp.org/logo.svg")]

#![allow(rustdoc::invalid_rust_codeblocks)]
#![allow(rustdoc::broken_intra_doc_links)]
#![allow(rustdoc::bare_urls)]
#![doc = include_str!("../README.md")]

use anyhow::Context as _;

use std::borrow::Borrow;
use std::collections::btree_map::{BTreeMap, Entry};
use std::fmt;
use std::fs::File;
use std::io;
use std::path::{Path, PathBuf};
use std::str::FromStr;
use std::sync::Mutex;
use std::time::SystemTime;
use std::sync::Arc;

use once_cell::sync::OnceCell;

use sequoia_openpgp as openpgp;

use openpgp::{
    KeyHandle,
    Result,
};
use openpgp::{armor, Cert};
use openpgp::cert::raw::RawCertParser;
use openpgp::crypto::Password;
use openpgp::Fingerprint;
use openpgp::packet::prelude::*;
use openpgp::parse::Parse;
use openpgp::packet::signature::subpacket::NotationData;
use openpgp::packet::signature::subpacket::NotationDataFlags;
use openpgp::serialize::Serialize;
use openpgp::cert::prelude::*;
use openpgp::policy::{Policy, StandardPolicy as P};
use openpgp::types::KeyFlags;
use openpgp::types::RevocationStatus;

use sequoia_cert_store as cert_store;
use cert_store::LazyCert;
use cert_store::Store;
use cert_store::store::StoreError;
use cert_store::store::StoreUpdate;
use cert_store::store::UserIDQueryParams;

use sequoia_wot as wot;
use wot::store::Store as _;

use sequoia_keystore as keystore;

use clap::FromArgMatches;

#[macro_use] mod macros;
#[macro_use] mod log;

mod common;
use common::PreferredUserID;
pub mod utils;

mod cli;
use cli::SECONDS_IN_DAY;
use cli::SECONDS_IN_YEAR;
use cli::types::Time;
use cli::output::{OutputFormat, OutputVersion};

mod commands;
pub mod output;
pub use output::{wkd::WkdUrlVariant, Model};
use output::hint::Hint;

/// Converts sequoia_openpgp types for rendering.
pub trait Convert<T> {
    /// Performs the conversion.
    fn convert(self) -> T;
}

impl Convert<humantime::FormattedDuration> for std::time::Duration {
    fn convert(self) -> humantime::FormattedDuration {
        humantime::format_duration(self)
    }
}

impl Convert<humantime::FormattedDuration> for openpgp::types::Duration {
    fn convert(self) -> humantime::FormattedDuration {
        humantime::format_duration(self.into())
    }
}

impl Convert<chrono::DateTime<chrono::offset::Utc>> for std::time::SystemTime {
    fn convert(self) -> chrono::DateTime<chrono::offset::Utc> {
        chrono::DateTime::<chrono::offset::Utc>::from(self)
    }
}

impl Convert<chrono::DateTime<chrono::offset::Utc>> for openpgp::types::Timestamp {
    fn convert(self) -> chrono::DateTime<chrono::offset::Utc> {
        std::time::SystemTime::from(self).convert()
    }
}

/// Loads one TSK from every given file.
fn load_keys<'a, I>(files: I) -> openpgp::Result<Vec<Cert>>
    where I: Iterator<Item=&'a Path>
{
    let mut certs = vec![];
    for f in files {
        let cert = Cert::from_file(f)
            .context(format!("Failed to load key from file {:?}", f))?;
        if ! cert.is_tsk() {
            return Err(anyhow::anyhow!(
                "Cert in file {:?} does not contain secret keys", f));
        }
        certs.push(cert);
    }
    Ok(certs)
}

/// Loads one or more certs from every given file.
fn load_certs<'a, I>(files: I) -> openpgp::Result<Vec<Cert>>
    where I: Iterator<Item=&'a Path>
{
    let mut certs = vec![];
    for f in files {
        for maybe_cert in CertParser::from_file(f)
            .context(format!("Failed to load certs from file {:?}", f))?
        {
            certs.push(maybe_cert.context(
                format!("A cert from file {:?} is bad", f)
            )?);
        }
    }
    Ok(certs)
}

/// Merges duplicate certs in a keyring.
fn merge_keyring<C>(certs: C) -> Result<BTreeMap<Fingerprint, Cert>>
where
    C: IntoIterator<Item = Cert>,
{
    let mut merged = BTreeMap::new();
    for cert in certs {
        match merged.entry(cert.fingerprint()) {
            Entry::Vacant(e) => {
                e.insert(cert);
            },
            Entry::Occupied(mut e) => {
                let old = e.get().clone();
                e.insert(old.merge_public(cert)?);
            },
        }
    }
    Ok(merged)
}

/// Serializes a keyring, adding descriptive headers if armored.
#[allow(dead_code)]
fn serialize_keyring(mut output: &mut dyn io::Write, certs: Vec<Cert>,
                     binary: bool)
                     -> openpgp::Result<()> {
    // Handle the easy options first.  No armor no cry:
    if binary {
        for cert in certs {
            cert.serialize(&mut output)?;
        }
        return Ok(());
    }

    // Just one Cert?  Ez:
    if certs.len() == 1 {
        return certs[0].armored().serialize(&mut output);
    }

    // Otherwise, merge the certs.
    let merged = merge_keyring(certs)?;

    // Then, collect the headers.
    let mut headers = Vec::new();
    for (i, cert) in merged.values().enumerate() {
        headers.push(format!("Key #{}", i));
        headers.append(&mut cert.armor_headers());
    }

    let headers: Vec<_> = headers.iter()
        .map(|value| ("Comment", value.as_str()))
        .collect();
    let mut output = armor::Writer::with_headers(&mut output,
                                                 armor::Kind::PublicKey,
                                                 headers)?;
    for cert in merged.values() {
        cert.serialize(&mut output)?;
    }
    output.finalize()?;
    Ok(())
}

/// Best-effort heuristic to compute the primary User ID of a given cert.
///
/// The returned string is already sanitized, and safe for displaying.
pub fn best_effort_primary_uid<'u, T>(config: Option<&Config>,
                                      cert: &'u Cert,
                                      policy: &'u dyn Policy,
                                      time: T)
                                      -> PreferredUserID
where
    T: Into<Option<SystemTime>>,
{
    let time = time.into();

    // Try to be more helpful by including a User ID in the
    // listing.  We'd like it to be the primary one.  Use
    // decreasingly strict policies.
    let mut primary_uid = None;

    // First, apply our policy.
    if let Ok(vcert) = cert.with_policy(policy, time) {
        if let Ok(primary) = vcert.primary_userid() {
            primary_uid = Some(primary.userid());
        }
    }

    // Second, apply the null policy.
    if primary_uid.is_none() {
        const NULL: openpgp::policy::NullPolicy =
            openpgp::policy::NullPolicy::new();
        if let Ok(vcert) = cert.with_policy(&NULL, time) {
            if let Ok(primary) = vcert.primary_userid() {
                primary_uid = Some(primary.userid());
            }
        }
    }

    // As a last resort, pick the first user id.
    if primary_uid.is_none() {
        if let Some(primary) = cert.userids().next() {
            primary_uid = Some(primary.userid());
        }
    }

    if let Some(primary_uid) = primary_uid {
        let fpr = cert.fingerprint();

        let mut candidate: (&UserID, usize) = (primary_uid, 0);

        #[allow(clippy::never_loop)]
        loop {
            // Don't fail if we can't query the user's web of trust.
            let Some(config) = config else { break; };
            let Ok(q) = config.wot_query() else { break; };
            let q = q.build();
            let authenticate = move |userid: &UserID| {
                let paths = q.authenticate(userid, &fpr, wot::FULLY_TRUSTED);
                paths.amount()
            };

            // We're careful to *not* use a ValidCert so that we see all
            // user IDs, even those that are not self signed.

            candidate = (primary_uid, authenticate(primary_uid));

            for userid in cert.userids() {
                let userid = userid.component();

                if candidate.1 >= wot::FULLY_TRUSTED {
                    // Done.
                    break;
                }

                if userid == primary_uid {
                    // We already considered this one.
                    continue;
                }

                let amount = authenticate(&userid);
                if amount > candidate.1 {
                    candidate = (userid, amount);
                }
            }

            break;
        }

        let (uid, amount) = candidate;
        PreferredUserID::from_userid(uid.clone(), amount)
    } else {
        // Special case, there is no user id.
        PreferredUserID::unknown()
    }
}

/// Best-effort heuristic to compute the primary User ID of a given cert.
///
/// The returned string is already sanitized, and safe for displaying.
pub fn best_effort_primary_uid_for<'u, T>(config: Option<&Config>,
                                          key_handle: &KeyHandle,
                                          policy: &'u dyn Policy,
                                          time: T)
                                          -> PreferredUserID
where
    T: Into<Option<SystemTime>>,
{
    let config = if let Some(config) = config {
        config
    } else {
        return PreferredUserID::unknown()
    };

    let cert = config.lookup_one(
        key_handle,
        Some(KeyFlags::empty()
             .set_storage_encryption()
             .set_transport_encryption()),
        false);

    match cert {
        Ok(cert) => {
            best_effort_primary_uid(Some(config), &cert, policy, time)
        }
        Err(err) => {
            if let Some(StoreError::NotFound(_))
                = err.downcast_ref()
            {
                PreferredUserID::from_string("(certificate not found)", 0)
            } else {
                PreferredUserID::from_string(
                    format!("(error looking up certificate: {})", err), 0)
            }
        }
    }
}

// Decrypts a key, if possible.
//
// The passwords in `passwords` are tried first.  If the key can't be
// decrypted using those, the user is prompted.  If a valid password
// is entered, it is added to `passwords`.
fn decrypt_key<R>(key: Key<key::SecretParts, R>, passwords: &mut Vec<Password>)
    -> Result<Key<key::SecretParts, R>>
    where R: key::KeyRole + Clone
{
    let key = key.parts_as_secret()?;
    match key.secret() {
        SecretKeyMaterial::Unencrypted(_) => {
            Ok(key.clone())
        }
        SecretKeyMaterial::Encrypted(e) => {
            if ! e.s2k().is_supported() {
                return Err(anyhow::anyhow!(
                    "Unsupported key protection mechanism"));
            }

            for p in passwords.iter() {
                if let Ok(key)
                    = key.clone().decrypt_secret(&p)
                {
                    return Ok(key);
                }
            }

            loop {
                // Prompt the user.
                match common::password::prompt_to_unlock_or_cancel(&format!(
                    "key {}", key.keyid(),
                )) {
                    Ok(None) => break, // Give up.
                    Ok(Some(p)) => {
                        if let Ok(key) = key
                            .clone()
                            .decrypt_secret(&p)
                        {
                            passwords.push(p.into());
                            return Ok(key);
                        }

                        wprintln!("Incorrect password.");
                    }
                    Err(err) => {
                        wprintln!("While reading password: {}", err);
                        break;
                    }
                }
            }

            Err(anyhow::anyhow!("Key {}: Unable to decrypt secret key material",
                                key.keyid().to_hex()))
        }
    }
}

/// Prints a warning if the user supplied "help" or "-help" to an
/// positional argument.
///
/// This should be used wherever a positional argument is followed by
/// an optional positional argument.
#[allow(dead_code)]
fn help_warning(arg: &str) {
    if arg == "help" {
        wprintln!("Warning: \"help\" is not a subcommand here.  \
                   Did you mean --help?");
    }
}

// A shorthand for our store type.
type WotStore<'store, 'rstore>
    = wot::store::CertStore<'store, 'rstore, cert_store::CertStore<'store>>;

pub struct Config<'store, 'rstore>
    where 'store: 'rstore
{
    verbose: bool,
    force: bool,
    output_format: OutputFormat,
    output_version: Option<OutputVersion>,
    policy: &'rstore P<'rstore>,
    time: SystemTime,
    // --no-cert-store
    no_rw_cert_store: bool,
    cert_store_path: Option<PathBuf>,
    pep_cert_store_path: Option<PathBuf>,
    keyrings: Vec<PathBuf>,
    // This will be set if --no-cert-store is not passed, OR --keyring
    // is passed.
    cert_store: OnceCell<WotStore<'store, 'rstore>>,

    // The value of --trust-root.
    trust_roots: Vec<Fingerprint>,
    // The local trust root, as set in the cert store.
    trust_root_local: OnceCell<Option<Fingerprint>>,

    // The key store.
    no_key_store: bool,
    key_store_path: Option<PathBuf>,
    key_store: OnceCell<Mutex<keystore::Keystore>>,
}

/// Whether a cert or key was freshly imported, updated, or unchanged.
///
/// Returned by [`Config::import_key`].
enum ImportStatus {
    /// The certificate or key is new.
    New,

    /// The certificate or key has been updated.
    Updated,

    /// The certificate or key is unchanged.
    Unchanged,
}

impl<'store: 'rstore, 'rstore> Config<'store, 'rstore> {
    /// Returns the cert store's base directory, if it is enabled.
    fn cert_store_base(&self) -> Option<PathBuf> {
        if self.no_rw_cert_store {
            None
        } else if let Some(path) = self.cert_store_path.as_ref() {
            Some(path.clone())
        } else {
            // XXX: openpgp-cert-d doesn't yet export this:
            // https://gitlab.com/sequoia-pgp/pgp-cert-d/-/issues/34
            // Remove this when it does.
            let pathbuf = dirs::data_dir()
                .expect("Unsupported platform")
                .join("pgp.cert.d");
            Some(pathbuf)
        }
    }

    /// Returns the cert store.
    ///
    /// If the cert store is disabled, returns `Ok(None)`.  If it is not yet
    /// open, opens it.
    fn cert_store(&self) -> Result<Option<&WotStore<'store, 'rstore>>> {
        if self.no_rw_cert_store
            && self.keyrings.is_empty()
            && self.pep_cert_store_path.is_none()
        {
            // The cert store is disabled.
            return Ok(None);
        }

        if let Some(cert_store) = self.cert_store.get() {
            // The cert store is already initialized, return it.
            return Ok(Some(cert_store));
        }

        let create_dirs = |path: &Path| -> Result<()> {
            use std::fs::DirBuilder;

            let mut b = DirBuilder::new();
            b.recursive(true);

            // Create the parent with the normal umask.
            if let Some(parent) = path.parent() {
                // Note: since recursive is turned on, it is not an
                // error if the directory exists, which is exactly
                // what we want.
                b.create(parent)
                    .with_context(|| {
                        format!("Creating the directory {:?}", parent)
                    })?;
            }

            // Create path with more restrictive permissions.
            platform!{
                unix => {
                    use std::os::unix::fs::DirBuilderExt;
                    b.mode(0o700);
                },
                windows => {
                },
            }

            b.create(path)
                .with_context(|| {
                    format!("Creating the directory {:?}", path)
                })?;

            Ok(())
        };

        // We need to initialize the cert store.
        let mut cert_store = if ! self.no_rw_cert_store {
            // Open the cert-d.

            let path = self.cert_store_base()
                .expect("just checked that it is configured");

            create_dirs(&path)
                .and_then(|_| cert_store::CertStore::open(&path))
                .with_context(|| {
                    format!("While opening the certificate store at {:?}",
                            &path)
                })?
        } else {
            cert_store::CertStore::empty()
        };

        let keyring = cert_store::store::Certs::empty();
        let mut error = None;
        for filename in self.keyrings.iter() {
            let f = std::fs::File::open(filename)
                .with_context(|| format!("Open {:?}", filename))?;
            let parser = RawCertParser::from_reader(f)
                .with_context(|| format!("Parsing {:?}", filename))?;

            for cert in parser {
                match cert {
                    Ok(cert) => {
                        keyring.update(Arc::new(cert.into()))
                            .expect("implementation doesn't fail");
                    }
                    Err(err) => {
                        eprint!("Parsing certificate in {:?}: {}",
                                filename, err);
                        error = Some(err);
                    }
                }
            }
        }

        if let Some(err) = error {
            return Err(err).context("Parsing keyrings");
        }

        cert_store.add_backend(
            Box::new(keyring),
            cert_store::AccessMode::Always);

        if let Some(ref pep_cert_store) = self.pep_cert_store_path {
            let pep_cert_store = if pep_cert_store.is_dir() {
                pep_cert_store.join("keys.db")
            } else {
                match pep_cert_store.try_exists() {
                    Ok(true) => {
                        PathBuf::from(pep_cert_store)
                    }
                    Ok(false) => {
                        return Err(anyhow::anyhow!(
                            "{:?} does not exist", pep_cert_store));
                    }
                    Err(err) => {
                        return Err(anyhow::anyhow!(
                            "Accessing {:?}: {}", pep_cert_store, err));
                    }
                }
            };

            let pep = cert_store::store::pep::Pep::open(Some(&pep_cert_store))?;

            cert_store.add_backend(
                Box::new(pep),
                cert_store::AccessMode::Always);
        }

        let cert_store = WotStore::from_store(
            cert_store, self.policy, self.time);

        let _ = self.cert_store.set(cert_store);

        Ok(Some(self.cert_store.get().expect("just configured")))
    }

    /// Returns the cert store.
    ///
    /// If the cert store is disabled, returns an error.
    fn cert_store_or_else(&self) -> Result<&WotStore<'store, 'rstore>> {
        self.cert_store().and_then(|cert_store| cert_store.ok_or_else(|| {
            anyhow::anyhow!("Operation requires a certificate store, \
                             but the certificate store is disabled")
        }))
    }

    /// Returns a mutable reference to the cert store.
    ///
    /// If the cert store is disabled, returns None.  If it is not yet
    /// open, opens it.
    fn cert_store_mut(&mut self)
        -> Result<Option<&mut WotStore<'store, 'rstore>>>
    {
        if self.no_rw_cert_store {
            return Err(anyhow::anyhow!(
                "Operation requires a certificate store, \
                 but the certificate store is disabled"));
        }

        // self.cert_store() will do any required initialization, but
        // it will return an immutable reference.
        self.cert_store()?;
        Ok(self.cert_store.get_mut())
    }

    /// Returns a mutable reference to the cert store.
    ///
    /// If the cert store is disabled, returns an error.
    #[allow(unused)]
    fn cert_store_mut_or_else(&mut self) -> Result<&mut WotStore<'store, 'rstore>> {
        self.cert_store_mut().and_then(|cert_store| cert_store.ok_or_else(|| {
            anyhow::anyhow!("Operation requires a certificate store, \
                             but the certificate store is disabled")
        }))
    }

    /// Returns a reference to the underlying certificate directory,
    /// if it is configured.
    ///
    /// If the cert direcgory is disabled, returns an error.
    fn certd_or_else(&self)
        -> Result<&cert_store::store::certd::CertD<'store>>
    {
        const NO_CERTD_ERR: &str =
            "A local trust root and other special certificates are \
             only available when using an OpenPGP certificate \
             directory";

        let cert_store = self.cert_store_or_else()
            .with_context(|| NO_CERTD_ERR.to_string())?;

        cert_store.certd()
            .ok_or_else(|| anyhow::anyhow!(NO_CERTD_ERR))
    }


    /// Returns a web-of-trust query builder.
    ///
    /// The trust roots are already set appropriately.
    fn wot_query(&self) -> Result<wot::QueryBuilder<&WotStore<'store, 'rstore>>>
    {
        let cert_store = self.cert_store_or_else()?;
        let network = wot::Network::new(cert_store)?;
        let mut query = wot::QueryBuilder::new_owned(network.into());
        query.roots(wot::Roots::new(self.trust_roots()));
        Ok(query)
    }

    /// Returns the key store's path.
    ///
    /// If the key store is disabled, returns `Ok(None)`.
    fn key_store_path(&self) -> Result<Option<PathBuf>> {
        if self.no_key_store {
            Ok(None)
        } else if let Some(dir) = self.key_store_path.as_ref() {
            Ok(Some(dir.clone()))
        } else if let Some(dir) = dirs::data_dir() {
            Ok(Some(dir.join("sequoia")))
        } else {
            Err(anyhow::anyhow!(
                "No key store, the XDG data directory is not defined"))
        }
    }

    /// Returns the key store's path.
    ///
    /// If the key store is disabled, returns an error.
    fn key_store_path_or_else(&self) -> Result<PathBuf> {
        const NO_KEY_STORE_ERROR: &str =
            "Operation requires a key store, \
             but the key store is disabled";

        if self.no_key_store {
            Err(anyhow::anyhow!(NO_KEY_STORE_ERROR))
        } else {
            self.key_store_path()?
                .ok_or(anyhow::anyhow!(NO_KEY_STORE_ERROR))
        }
    }

    /// Returns the key store.
    ///
    /// If the key store is disabled, returns `Ok(None)`.  If it is not yet
    /// open, opens it.
    fn key_store(&self) -> Result<Option<&Mutex<keystore::Keystore>>> {
        if self.no_key_store {
            return Ok(None);
        }

        self.key_store
            .get_or_try_init(|| {
                let c = keystore::Context::configure()
                    .home(self.key_store_path_or_else()?)
                    .build()?;
                let ks = keystore::Keystore::connect(&c)
                    .context("Connecting to key store")?;

                Ok(Mutex::new(ks))
            })
            .map(Some)
    }

    /// Returns the key store.
    ///
    /// If the key store is disabled, returns an error.
    fn key_store_or_else(&self) -> Result<&Mutex<keystore::Keystore>> {
        self.key_store().and_then(|key_store| key_store.ok_or_else(|| {
            anyhow::anyhow!("Operation requires a key store, \
                             but the key store is disabled")
        }))
    }

    /// Looks up an identifier.
    ///
    /// This matches on both the primary key and the subkeys.
    ///
    /// If `keyflags` is not `None`, then only returns certificates
    /// where the matching key has at least one of the specified key
    /// flags.  If `or_by_primary` is set, then certificates with the
    /// specified key handle and a subkey with the specified flags
    /// also match.
    ///
    /// If `allow_ambiguous` is true, then all matching certificates
    /// are returned.  Otherwise, if an identifier matches multiple
    /// certificates an error is returned.
    ///
    /// An error is also returned if any of the identifiers does not
    /// match at least one certificate.
    fn lookup<'a, I>(&self, khs: I,
                     keyflags: Option<KeyFlags>,
                     or_by_primary: bool,
                     allow_ambiguous: bool)
              -> Result<Vec<Cert>>
    where I: IntoIterator,
          I::Item: Borrow<KeyHandle>,
    {
        let mut results = Vec::new();

        for kh in khs {
            let kh = kh.borrow();
            match self.cert_store_or_else()?.lookup_by_cert_or_subkey(&kh) {
                Err(err) => {
                    let err = anyhow::Error::from(err);
                    return Err(err.context(
                        format!("Failed to load {} from certificate store", kh)
                    ));
                }
                Ok(certs) => {
                    let mut certs = certs.into_iter()
                        .filter_map(|cert| {
                            match cert.to_cert() {
                                Ok(cert) => Some(cert.clone()),
                                Err(err) => {
                                    let err = err.context(
                                        format!("Failed to parse {} as loaded \
                                                 from certificate store", kh));
                                    print_error_chain(&err);
                                    None
                                }
                            }
                        })
                        .collect::<Vec<Cert>>();

                    if let Some(keyflags) = keyflags.as_ref() {
                        certs.retain(|cert| {
                            let vc = match cert.with_policy(
                                self.policy, self.time)
                            {
                                Ok(vc) => vc,
                                Err(err) => {
                                    let err = err.context(
                                        format!("{} is not valid according \
                                                 to the current policy, ignoring",
                                                kh));
                                    print_error_chain(&err);
                                    return false;
                                }
                            };

                            let checked_id = or_by_primary
                                && vc.key_handle().aliases(kh);

                            for ka in vc.keys() {
                                if checked_id || ka.key_handle().aliases(kh) {
                                    if &ka.key_flags().unwrap_or(KeyFlags::empty())
                                        & keyflags
                                        != KeyFlags::empty()
                                    {
                                        return true;
                                    }
                                }
                            }

                            if checked_id {
                                wprintln!("Error: {} does not have a key with \
                                           the required capabilities ({:?})",
                                          cert.keyid(), keyflags);
                            } else {
                                wprintln!("Error: The subkey {} (cert: {}) \
                                           does not the required capabilities \
                                           ({:?})",
                                          kh, cert.keyid(), keyflags);
                            }
                            return false;
                        })
                    }

                    if ! allow_ambiguous && certs.len() > 1 {
                        return Err(anyhow::anyhow!(
                            "{} is ambiguous; it matches: {}",
                            kh,
                            certs.into_iter()
                                .map(|cert| cert.fingerprint().to_string())
                                .collect::<Vec<String>>()
                                .join(", ")));
                    }

                    if certs.len() == 0 {
                        return Err(StoreError::NotFound(kh.clone()).into());
                    }

                    results.extend(certs);
                }
            }
        }

        Ok(results)
    }

    /// Looks up certificates by User ID or email address.
    ///
    /// This only returns certificates that can be authenticate for
    /// the specified User ID (or email address, if `email` is true).
    /// If no certificate can be authenticated for some User ID,
    /// returns an error.  If multiple certificates can be
    /// authenticated for a given User ID or email address, then
    /// returns them all.
    fn lookup_by_userid(&self, userid: &[String], email: bool)
        -> Result<Vec<Cert>>
    {
        if userid.is_empty() {
            return Ok(Vec::new())
        }

        let cert_store = self.cert_store_or_else()?;

        // Build a WoT network.

        let cert_store = wot::store::CertStore::from_store(
            cert_store, self.policy, self.time);
        let n = wot::Network::new(&cert_store)?;
        let mut q = wot::QueryBuilder::new(&n);
        q.roots(wot::Roots::new(self.trust_roots().iter().cloned()));
        let q = q.build();

        let mut results: Vec<Cert> = Vec::new();
        // We try hard to not just stop at the first error, but lint
        // the input so that the user gets as much feedback as
        // possible.  The first error that we encounter is saved here,
        // and returned.  The rest are printed directly.
        let mut error: Option<anyhow::Error> = None;

        // Iterate over each User ID address, find any certificates
        // associated with the User ID, validate the certificates, and
        // finally authenticate them for the User ID.
        for userid in userid.iter() {
            let matches: Vec<(Fingerprint, UserID)> = if email {
                if let Err(err) = UserIDQueryParams::is_email(userid) {
                    wprintln!("{:?} is not a valid email address", userid);
                    if error.is_none() {
                        error = Some(err);
                    }

                    continue;
                }

                // Get all certificates that are associated with the email
                // address.
                cert_store.lookup_synopses_by_email(userid)
            } else {
                let userid = UserID::from(&userid[..]);
                cert_store.lookup_synopses_by_userid(userid.clone())
                    .into_iter()
                    .map(|fpr| (fpr, userid.clone()))
                    .collect()
            };

            if matches.is_empty() {
                return Err(anyhow::anyhow!(
                    "No certificates are associated with {:?}",
                    userid));
            }

            struct Entry {
                fpr: Fingerprint,
                userid: UserID,
                cert: Result<Cert>,
            }
            let entries = matches.into_iter().map(|(fpr, userid)| {
                // We've got a match, or two, or three...  Lookup the certs.
                let cert = match cert_store.lookup_by_cert_fpr(&fpr) {
                    Ok(cert) => cert,
                    Err(err) => {
                        let err = err.context(format!(
                            "Error fetching {} ({:?})",
                            fpr, String::from_utf8_lossy(userid.value())));
                        return Entry { fpr, userid, cert: Err(err), };
                    }
                };

                // Parse the LazyCerts.
                let cert = match cert.to_cert() {
                    Ok(cert) => cert.clone(),
                    Err(err) => {
                        let err = err.context(format!(
                            "Error parsing {} ({:?})",
                            fpr, String::from_utf8_lossy(userid.value())));
                        return Entry { fpr, userid, cert: Err(err), };
                    }
                };

                // Check the certs for validity.
                let vc = match cert.with_policy(self.policy, self.time) {
                    Ok(vc) => vc,
                    Err(err) => {
                        let err = err.context(format!(
                            "Certificate {} ({:?}) is invalid",
                            fpr, String::from_utf8_lossy(userid.value())));
                        return Entry { fpr, userid, cert: Err(err) };
                    }
                };

                if let Err(err) = vc.alive() {
                    let err = err.context(format!(
                        "Certificate {} ({:?}) is invalid",
                        fpr, String::from_utf8_lossy(userid.value())));
                    return Entry { fpr, userid, cert: Err(err), };
                }

                if let RevocationStatus::Revoked(_) = vc.revocation_status() {
                    let err = anyhow::anyhow!(
                        "Certificate {} ({:?}) is revoked",
                        fpr, String::from_utf8_lossy(userid.value()));
                    return Entry { fpr, userid, cert: Err(err), };
                }

                if let Some(ua) = vc.userids().find(|ua| {
                    ua.userid() == &userid
                })
                {
                    if let RevocationStatus::Revoked(_) = ua.revocation_status() {
                        let err = anyhow::anyhow!(
                            "User ID {:?} on certificate {} is revoked",
                            String::from_utf8_lossy(userid.value()), fpr);
                        return Entry { fpr, userid, cert: Err(err), };
                    }
                }

                // Authenticate the bindings.
                let paths = q.authenticate(
                    &userid, cert.fingerprint(),
                    // XXX: Make this user configurable.
                    wot::FULLY_TRUSTED);
                let r = if paths.amount() < wot::FULLY_TRUSTED {
                    Err(anyhow::anyhow!(
                        "{}, {:?} cannot be authenticated at the \
                         required level ({} of {}).  After checking \
                         that {} really controls {}, you could certify \
                         their certificate by running \
                         `sq link add {} {:?}`.",
                        cert.fingerprint(),
                        String::from_utf8_lossy(userid.value()),
                        paths.amount(), wot::FULLY_TRUSTED,
                        String::from_utf8_lossy(userid.value()),
                        cert.fingerprint(),
                        cert.fingerprint(),
                        String::from_utf8_lossy(userid.value())))
                } else {
                    Ok(cert)
                };

                Entry { fpr, userid, cert: r, }
            });

            // Partition into good (successfully authenticated) and
            // bad (an error occurred).
            let (good, bad): (Vec<Entry>, _)
                = entries.partition(|entry| entry.cert.is_ok());

            if good.is_empty() {
                // We've only got errors.

                let err = if bad.is_empty() {
                    // We got nothing :/.
                    if email {
                        anyhow::anyhow!(
                            "No known certificates have the email address {:?}",
                            userid)
                    } else {
                        anyhow::anyhow!(
                            "No known certificates have the User ID {:?}",
                            userid)
                    }
                } else {
                    if email {
                        anyhow::anyhow!(
                            "None of the certificates with the email \
                             address {:?} can be authenticated using \
                             the configured trust model",
                            userid)
                    } else {
                        anyhow::anyhow!(
                            "None of the certificates with the User ID \
                             {:?} can be authenticated using \
                             the configured trust model",
                            userid)
                    }
                };

                wprintln!("{:?}:\n", err);
                if error.is_none() {
                    error = Some(err);
                }

                // Print the errors.
                for (i, Entry { fpr, userid, cert }) in bad.into_iter().enumerate() {
                    wprintln!("{}. When considering {} ({}):",
                              i + 1, fpr,
                              String::from_utf8_lossy(userid.value()));
                    let err = match cert {
                        Ok(_) => unreachable!(),
                        Err(err) => err,
                    };

                    print_error_chain(&err);
                }
            } else {
                // We have at least one authenticated certificate.
                // Silently ignore any errors.
                results.extend(
                    good.into_iter().filter_map(|Entry { cert, .. }| {
                        cert.ok()
                    }));
            }
        }

        if let Some(error) = error {
            Err(error)
        } else {
            Ok(results)
        }
    }


    /// Looks up a certificate.
    ///
    /// Like `lookup`, but looks up a certificate, which must be
    /// uniquely identified by `kh` and `keyflags`.
    fn lookup_one(&self, kh: &KeyHandle,
                  keyflags: Option<KeyFlags>, or_by_primary: bool)
        -> Result<Cert>
    {
        self.lookup(std::iter::once(kh), keyflags, or_by_primary, false)
            .map(|certs| {
                assert_eq!(certs.len(), 1);
                certs.into_iter().next().expect("have one")
            })
    }

    /// Returns the local trust root, creating it if necessary.
    fn local_trust_root(&self) -> Result<Arc<LazyCert<'store>>> {
        self.certd_or_else()?.trust_root().map(|(cert, _created)| {
            cert
        })
    }

    /// Returns the trust roots, including the cert store's trust
    /// root, if any.
    fn trust_roots(&self) -> Vec<Fingerprint> {
        let trust_root_local = self.trust_root_local.get_or_init(|| {
            self.cert_store_or_else()
                .ok()
                .and_then(|cert_store| cert_store.certd())
                .and_then(|certd| {
                    match certd.certd().get(cert_store::store::openpgp_cert_d::TRUST_ROOT) {
                        Ok(Some((_tag, cert_bytes))) => Some(cert_bytes),
                        // Not found.
                        Ok(None) => None,
                        Err(err) => {
                            wprintln!("Error looking up local trust root: {}",
                                      err);
                            None
                        }
                    }
                })
                .and_then(|cert_bytes| {
                    match RawCertParser::from_bytes(&cert_bytes[..]) {
                        Ok(mut parser) => {
                            match parser.next() {
                                Some(Ok(cert)) => Some(cert.fingerprint()),
                                Some(Err(err)) => {
                                    wprintln!("Local trust root is \
                                               corrupted: {}",
                                              err);
                                    None
                                }
                                None =>  {
                                    wprintln!("Local trust root is \
                                               corrupted: no data");
                                    None
                                }
                            }
                        }
                        Err(err) => {
                            wprintln!("Error parsing local trust root: {}",
                                      err);
                            None
                        }
                    }
                })
        });

        if let Some(trust_root_local) = trust_root_local {
            self.trust_roots.iter().cloned()
                .chain(std::iter::once(trust_root_local.clone()))
                .collect()
        } else {
            self.trust_roots.clone()
        }
    }

    /// Imports the TSK into the soft key backend.
    ///
    /// On success, returns whether the key was imported, updated, or
    /// unchanged.
    fn import_key(&self, mut cert: Cert) -> Result<ImportStatus> {
        if ! cert.is_tsk() {
            return Err(anyhow::anyhow!(
                "Certificate does not contain any secret key material"));
        }

        let softkeys = self.key_store_path_or_else()?
            .join("keystore").join("softkeys");

        std::fs::create_dir_all(&softkeys)?;

        let fpr = cert.fingerprint();

        let filename = softkeys.join(format!("{}.pgp", fpr));

        let mut update = false;
        match Cert::from_file(&filename) {
            Ok(old) => {
                if old.fingerprint() != fpr {
                    return Err(anyhow::anyhow!(
                        "{} contains {}, but expected {}",
                        filename.display(),
                        old.fingerprint(),
                        fpr));
                }

                update = true;

                // Prefer secret key material from `cert`.
                cert = old.clone().merge_public_and_secret(cert.clone())?;

                if cert == old {
                    return Ok(ImportStatus::Unchanged);
                }
            }
            Err(err) => {
                // If the file doesn't exist yet, it's not an
                // error: it just means that we don't have to
                // merge.
                if let Some(ioerr) = err.downcast_ref::<std::io::Error>() {
                    if ioerr.kind() == std::io::ErrorKind::NotFound {
                        // Not found.  No problem.
                    } else {
                        return Err(err);
                    }
                } else {
                    return Err(err);
                }
            }
        }

        // We write to a temporary file and then move it into
        // place.  This doesn't eliminate races, but it does
        // prevent a partial update from destroying the existing
        // data.
        let mut tmp_filename = filename.clone();
        tmp_filename.set_extension("pgp~");

        let mut f = File::create(&tmp_filename)?;
        cert.as_tsk().serialize(&mut f)?;

        std::fs::rename(&tmp_filename, &filename)?;

        // Also insert the certificate into the certificate store.
        // If we can't, we don't fail.  This allows, in
        // particular, `sq --no-cert-store key import` to work.
        match self.cert_store_or_else() {
            Ok(cert_store) => {
                if let Err(err) = cert_store.update(
                    Arc::new(LazyCert::from(cert)))
                {
                    self.info(format_args!(
                        "While importing {} into cert store: {}",
                        fpr, err));
                }
            }
            Err(err) => {
                self.info(format_args!(
                    "Not importing {} into cert store: {}",
                    fpr, err));
            }
        }

        if update {
            return Ok(ImportStatus::Updated);
        } else {
            return Ok(ImportStatus::New);
        }
    }

    /// Prints additional information in verbose mode.
    fn info(&self, msg: fmt::Arguments) {
        if self.verbose {
            wprintln!("{}", msg);
        }
    }

    /// Prints a hint for the user.
    fn hint(&self, msg: fmt::Arguments) -> Hint {
        // XXX: If we gain a --quiet, pass it to Hint::new.
        Hint::new(false)
            .hint(msg)
    }
}

// TODO: Use `derive`d command structs. No more values_of
// TODO: Handling (and cli position) of global arguments
fn main() -> Result<()> {
    let mut cli = cli::build(true);
    let matches = cli.clone().try_get_matches();
    let c = match matches {
        Ok(matches) => {
            cli::SqCommand::from_arg_matches(&matches)?
        }
        Err(err) => {
            // Warning: hack ahead!
            //
            // If we are showing the help output, we only want to
            // display the global options at the top-level; for
            // subcommands we hide the global options to not overwhelm
            // the user.
            //
            // Ideally, clap would provide a mechanism to only show
            // the help output for global options at the level they
            // are defined at.  That's not the case.
            //
            // We can use `err` to figure out if we are showing the
            // help output, but it doesn't tell us what subcommand we
            // are showing the help for.  Instead (and here's the
            // hack!), we compare the output.  If it is the output for
            // the top-level `--help` or `-h`, then we are showing the
            // help for the top-level.  If not, then we are showing
            // the help for a subcommand.  In the former case, we
            // unhide the global options.
            use clap::error::ErrorKind;
            if err.kind() == ErrorKind::DisplayHelp
                || err.kind() == ErrorKind::DisplayHelpOnMissingArgumentOrSubcommand
            {
                let output = err.render();
                let output = if output == cli.render_long_help() {
                    Some(cli::build(false).render_long_help())
                } else if output == cli.render_help() {
                    Some(cli::build(false).render_help())
                } else {
                    None
                };

                if let Some(output) = output {
                    if err.use_stderr() {
                        eprint!("{}", output);
                    } else {
                        print!("{}", output);
                    }
                    std::process::exit(err.exit_code());
                }
            }
            err.exit();
        }
    };

    let time: SystemTime =
        c.time.clone().unwrap_or_else(|| Time::now()).into();

    let mut policy = sequoia_policy_config::ConfiguredStandardPolicy::new();
    policy.parse_default_config()?;
    let mut policy = policy.build();

    let known_notations_store = c.known_notation.clone();
    let known_notations = known_notations_store
        .iter()
        .map(|n| n.as_str())
        .collect::<Vec<&str>>();
    policy.good_critical_notations(&known_notations);

    let force = c.force;

    let output_version = if let Some(v) = &c.output_version {
        Some(OutputVersion::from_str(v)?)
    } else {
        None
    };

    let config = Config {
        verbose: c.verbose,
        force,
        output_format: c.output_format,
        output_version,
        policy: &policy,
        time,
        no_rw_cert_store: c.no_cert_store,
        cert_store_path: c.cert_store.clone(),
        pep_cert_store_path: c.pep_cert_store.clone(),
        keyrings: c.keyring.clone(),
        cert_store: OnceCell::new(),
        trust_roots: c.trust_roots.clone(),
        trust_root_local: Default::default(),
        no_key_store: c.no_key_store,
        key_store_path: c.key_store.clone(),
        key_store: OnceCell::new(),
    };

    commands::dispatch(config, c)
}

fn parse_notations<N>(n: N) -> Result<Vec<(bool, NotationData)>>
where
    N: AsRef<[String]>,
{
    let n = n.as_ref();
    assert_eq!(n.len() % 2, 0, "notations must be pairs of key and value");

    // Each --notation takes two values.  Iterate over them in chunks of 2.
    let notations: Vec<(bool, NotationData)> = n
        .chunks(2)
        .map(|arg_pair| {
            let name = &arg_pair[0];
            let value = &arg_pair[1];

            let (critical, name) = match name.strip_prefix('!') {
                Some(name) => (true, name),
                None => (false, name.as_str()),
            };

            let notation_data = NotationData::new(
                name,
                value,
                NotationDataFlags::empty().set_human_readable(),
            );
            (critical, notation_data)
        })
        .collect();

    Ok(notations)
}

// Sometimes the same error cascades, e.g.:
//
// ```
// $ sq-wot --time 20230110T0406   --keyring sha1.pgp path B5FA089BA76FE3E17DC11660960E53286738F94C 231BC4AB9D8CAB86D1622CE02C0CE554998EECDB FABA8485B2D4D5BF1582AA963A8115E774FA9852 "<carol@example.org>"
// [ ] FABA8485B2D4D5BF1582AA963A8115E774FA9852 <carol@example.org>: not authenticated (0%)
//   ◯ B5FA089BA76FE3E17DC11660960E53286738F94C ("<alice@example.org>")
//   │   No adequate certification found.
//   │   No binding signature at time 2023-01-10T04:06:00Z
//   │     No binding signature at time 2023-01-10T04:06:00Z
//   │     No binding signature at time 2023-01-10T04:06:00Z
// ...
// ```
//
// Compress these.
fn error_chain(err: &anyhow::Error) -> Vec<String> {
    let mut errs = std::iter::once(err.to_string())
        .chain(err.chain().map(|source| source.to_string()))
        .collect::<Vec<String>>();
    errs.dedup();
    errs
}

/// Prints the error and causes, if any.
pub fn print_error_chain(err: &anyhow::Error) {
    wprintln!("           {}", err);
    err.chain().skip(1).for_each(|cause| wprintln!("  because: {}", cause));
}

/// Returns the error chain as a string.
///
/// The error and causes are separated by `error_separator`.  The
/// causes are separated by `cause_separator`, or, if that is `None`,
/// `error_separator`.
pub fn display_error_chain<'a, E, C>(err: E,
                                     error_separator: &str,
                                     cause_separator: C)
    -> String
where E: Borrow<anyhow::Error>,
      C: Into<Option<&'a str>>
{
    let err = err.borrow();
    let cause_separator = cause_separator.into();

    let error_chain = error_chain(err);
    match error_chain.len() {
        0 => unreachable!(),
        1 => {
            error_chain.into_iter().next().expect("have one")
        }
        2 => {
            format!("{}{}{}",
                    error_chain[0],
                    error_separator,
                    error_chain[1])
        }
        _ => {
            if let Some(cause_separator) = cause_separator {
                format!("{}{}{}",
                        error_chain[0],
                        error_separator,
                        error_chain[1..].join(cause_separator))
            } else {
                error_chain.join(error_separator)
            }
        }
    }

}

pub fn one_line_error_chain<E>(err: E) -> String
where E: Borrow<anyhow::Error>,
{
    display_error_chain(err, ": ", ", because ")
}
